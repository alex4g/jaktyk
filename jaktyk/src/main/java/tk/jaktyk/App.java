package tk.jaktyk;

import javax.swing.JFrame;

/**
 * Main Program for running the bouncing ball as a standalone application.
 * 
 * @author Alex Shyrin thanks @author Hock-Chuan Chua
 * @version 10 April 2015
 */

public class App 
{
	// Entry main program
	public static void main(String[] args) {
		// Run UI in the Event Dispatcher Thread (EDT), instead of Main thread
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame("JaKTyK");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setContentPane(new BallWorld(640, 480)); // BallWorld is a JPanel
				frame.pack();            // Preferred size of BallWorld
				frame.setVisible(true);  // Show it
			}
		});
	}
}
